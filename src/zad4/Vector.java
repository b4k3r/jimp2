package zad4;

import java.util.ArrayList;

public class Vector extends Object {
    private ArrayList<Element> vector;

    public Vector(Element[] el) {
        vector = new ArrayList<>();
        for (Element e : el) {
            vector.add(e);
        }
    }

    // Tworzymy Vector tylko z jednym elementem
    public Vector(Element ele) {
        vector = new ArrayList<>();
        vector.add(ele);
    }

    public void addElement(Element ele) {
        vector.add(ele);
    }

    // Zwraca ostani element i go usuwa
    public Element getLastElement() {
        return vector.remove(vector.size() - 1);
    }

    public Element getElement(int i) {
        return vector.get(i);
    }

    public int getSize() {
        return vector.size();
    }

    @Override
    public String toString() {
        return getClass().getName() + vector.toString() ;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (Element e : vector) {
            hash += e.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null) return false;

        if (getClass() != o.getClass()) return false;

        Vector v = (Vector) o;

        if (v.getSize() != getSize()) return false;

        for (int i = 0; i < getSize(); i++)
            if (!getElement(i).equals(v.getElement(i)))
                return false;

        return true;
    }
}
