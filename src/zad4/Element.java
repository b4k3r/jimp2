package zad4;

public class Element implements Comparable<Element> {
    private String name;

    public Element(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Element el){
        return name.compareTo(el.getName());
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null) return false;

        if (getClass() != o.getClass()) return false;

        Element el = (Element) o;

        return name.equals(el.name);
    }
}
