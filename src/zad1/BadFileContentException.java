package zad1;

class BadFileContentException extends Exception
{
    public BadFileContentException() {}
    public BadFileContentException(String gripe)
    {
        super(gripe);
    }
}
