package zad1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Test {
    public static void main(String args[]) {
        try {
            ArrayList<Double> vector;
            vector = readVector("gowno.txt");
            System.out.println(vector.toString());
        } catch (BadFileContentException e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Double> readVector(String Filename) throws BadFileContentException {
        String line;
        BufferedReader reader = null;
        int n = 2;

        try {
            reader = new BufferedReader(new FileReader(Filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Czytamy pierwszy wiersz i parsujemy go na int
        int count;
        try {
            line = reader.readLine();
            count = Integer.parseInt(line);
        } catch (IOException | NumberFormatException e) {
            throw new BadFileContentException("Blad w " + Filename + " w lini 1. Nieprawidlowa wielkosc wektora.");
        }

        // Tworzymy wektor o dlugosci count
        ArrayList<Double> vector = new ArrayList<>(count);

        try {
            while ((line = reader.readLine()) != null) {
                vector.add(Double.parseDouble(line));
                n++;
            }
        // W sumie można to rozjebać jeszcze na dwa przypadki IOex... i Number... Tylko po chuj
        } catch (IOException | NumberFormatException e) {
            throw new BadFileContentException("Blad w " + Filename + " w lini " + n + ". Nieprawidlowa liczba wektora.");
        }

        return vector;
    }
}
