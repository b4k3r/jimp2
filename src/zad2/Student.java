package zad2;

public class Student {
    private State state;

    public Student() {
    }

    public Student(State state) {
        this.state = state;
    }

    // Ustalamy stan studenta
    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    /* A następnie wywyłujemy metodę, która wywyłuje metodę fuckStudent na rzecz tego stanu
     i przekazuje do niej siebie, czli klasę Student (jest to this)
     Dzięki temu wzorcowi nie musimy pierdoliść się z ifami, czli np.
        if stan == chuj
            zrob cos
        else if (stan == kutas)
            zrob cos
        ...
        end
    */
    public void fuckStudent() {
        state.fuckStudent(this);
    }
}
