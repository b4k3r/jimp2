package zad3;

public class Student extends Object implements Comparable<Student> {
    private String name;
    private String surname;

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public int compareTo(Student s) {
        int nameCompare = name.compareTo(s.getName());
        if (nameCompare == 0) {
            return surname.compareTo(s.getSurname());
        } else {
            return nameCompare;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null) return false;

        if (getClass() != o.getClass()) return false;

        Student s = (Student) o;

        return name.equals(s.getName())
                && surname.equals(s.getSurname());
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }
}
