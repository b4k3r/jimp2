package zad3;

public class Test {
    public static void main(String[] args) {
        Student pizda1 = new Student("Wojciech", "Buczek");
        Student pizda2 = new Student("Piotr", "Zawada");

        // Test compare
        System.out.println(pizda1.compareTo(pizda1));
        System.out.println(pizda1.compareTo(pizda2));

        // Test equals
        System.out.println(pizda1.equals(pizda1));
        System.out.println(pizda1.equals(pizda2));
        System.out.println(pizda1.equals(new Object()));

        // Test toString
        System.out.println(pizda1.toString());
        System.out.println(pizda2.toString());

    }
}
